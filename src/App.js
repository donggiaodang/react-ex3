import logo from './logo.svg';
import './App.css';
import ExShoeMain from './ExShoeShop/ExShoeMain';

function App() {
  return (
    <div className="App">
      <ExShoeMain/>
    </div>
  );
}

export default App;
