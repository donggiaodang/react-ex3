import React, { Component } from 'react'

export default class DetailShoe extends Component {
  render() {
    let {image, name, description,id, price}= this.props.detail
    return (
      <div className='row mt-5 alert-secondary p-5 text-left '>
        
            <img src={image} alt="" className='col-3 ' />
    
        <div className="col-9">
            <p>ID: {id}  </p>
            <p>Name: {name} </p>
            <p>Price: {price} </p>
            <p>Description:{description} </p>
        </div>
      </div>
    )
  }
}
