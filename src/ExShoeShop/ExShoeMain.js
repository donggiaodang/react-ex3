import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './dataShoe'
import DetailShoe from './DetailShoe'
import ListShoe from './ListShoe'

export default class ExShoeMain extends Component {
    state ={
      shoeArr: dataShoe,
      detail: dataShoe[0],
      cart: [],  
    }
    decreaseAmount = (shoe)=>{
        let cloneCart = [...this.state.cart]
        // find index 
        let index = this.state.cart.findIndex((item)=>{
            return item.id == shoe.id;

        })
        if(cloneCart[index].number==1){
            cloneCart.splice(index,1)
        }else{
            cloneCart[index].number--
        }
        this.setState({
            cart: cloneCart,
        })
    }
    increaseAmount=(shoe)=>{
       
        let cloneCart = [...this.state.cart]
        // find index 
        let index = this.state.cart.findIndex((item)=>{
            return item.id == shoe.id;

        })
        cloneCart[index].number++
        this.setState({
            cart: cloneCart,
        })
    }
    handleAddToCart =(shoe)=>{
        let cloneCart = [...this.state.cart]
        // find index 
        let index = this.state.cart.findIndex((item)=>{
            return item.id == shoe.id;

        })
        if(index ==-1){
            let newItem = {...shoe, number:1}
            cloneCart.push(newItem)
        }else{
            cloneCart[index].number++
        }
        this.setState({
            cart: cloneCart,
        })
    }
    handleDetailShoe=(shoe)=>{
        this.setState({
            detail: shoe,
        })
    }
    render() {
        
        return (
        
      <div className='container py-5'>
        <Cart 
        cart ={this.state.cart}
        increaseAmount ={this.increaseAmount}
        decreaseAmount = {this.decreaseAmount}
       />
        <ListShoe 
        handleDetailShoe = {this.handleDetailShoe}
        shoeArr ={this.state.shoeArr}
        handleAddToCart = {this.handleAddToCart}/>
        <DetailShoe detail = {this.state.detail}/>
      </div>
    )
  }
}
